# Blender add-on setup

Apart from the [Remote Console](basic-setup.md#accessing-the-remote-console), another beginner-friendly option for managing Chordata Motion's system and captures, is our [Blender](https://blender.org) Add-on. Leveraging Blender 3D capabilities, Chordata Motion add-on offers a complete toolkit for managing, inspecting and recording captures.

## Add-on installation

Follow these steps to install Chordata Motion Blender add-on:

1. Download and install [Blender](https://www.blender.org/download/).
2. Download the add-on from our [downloads page](https://chordata.cc/downloads/).
3. To install and activate the add-on:
    - Launch Blender.
    - Go to "Edit" > "Preferences..." > "Add-ons".
    - Click the "Install..." button.
    - Select the Chordata Motion add-on .zip file you've downloaded. 
    - Once installed, the add-on will appear in Blender's add-ons list.
    - Tick the box next to the add-on's name in order to activate it.

