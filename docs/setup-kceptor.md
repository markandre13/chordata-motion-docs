# KCeptor setup

The main sensor of Chordata Motion system, the **KCeptor**, is what is known as an **inertial sensor**. While bringing many advantages, these types of sensors need to undergo a **one-time calibration**. Through this process, the deviations that are inherent to this type of technology are calculated and stored in an internal memory of the KCeptor, so that they can be compensated.

## KCeptor calibration
*Estimated time: 4 minutes / sensor*

To perform sensor calibration, you will need to repeat a specific **sequence of movements** for each KCeptor. It's important to perform these actions **carefully**, since the quality of the capture depends on the data obtained from each sensor.

!!! note
	Sensor calibration is a one-time procedure. Each KCeptor needs to be calibrated **before its first use**.
	Recalibration is needed only if the sensor suffered from mechanical shocks or drastic changes in temperature.

!!! warning
	It's **higly recommended** to perform the sensor calibration after [mounting the KCeptor to the socket](#mount-kceptor), since this could affect the results of the calibration.

### KCeptor calibration with Chordata Remote Console

The [Chordata Remote Console](basic-setup.md#accessing-the-remote-console) offers a convenient interface for executing sensor calibration, following these steps:

1. **Connect ONE KCeptor** to the Hub++.
2. Place the KCeptor on a **flat surface**, with its connectors facing down and make sure it's **completely still**.

	![KCeptor++ in a flat surface](img/kceptorpp/kceptor_flat_surface.jpg)

3. In the Remote Console, press "**Start calibration**".
4. Once the first part of the calibration is completed, you will be prompted to get ready for the magnetic calibration procedure.
5. **Raise the KCeptor** from the surface and hold it in your hand  **as far as posible from other objects**, specifically metallic objects or sources of magnetism (laptop, speakers, metal screws, etc.).  
	
	<video  autoplay="true" loop="true">
		<source src="../img/kceptorpp/up_animation.mp4" type="video/mp4">
	</video>



6. Once you're ready, you'll need to press "**Mag Calib**" in the Remote Console and start rotating your KCeptor in every possible orientation. Imagine the KCeptor as a cube and **repeat these steps for each face** of the cube:  
	- rotate the KCeptor to make the **current face pointing upwards**.
	- rotate the KCeptor by 360º on its vertical axis, while keeping the current face pointing upwards.

	<video  autoplay="true" loop="true">
		<source src="../img/kceptorpp/rotation_calibration_animation.mp4" type="video/mp4">
	</video>

7. Once you're done, press "**Finish Calib**" in the Remote Console.

8. Done!

### Test KCeptor calibration

The KCeptor calibration can be tested with the [Chordata Blender Add-on](blender-addon.md). Activate the add-on and follow these steps:

1. **Connect ONE KCeptor** to the Hub++.
2. In Blender, "**Shift + A**" > **Chordata** > **Add a KCeptor model**.
3. Switch to the Mocap workspace.
4. In the node tree, tick "**Scan**" in the **Notochord node** and press "**Connect**".
4. Look for the current KCeptor name in the **Test Cube node**, and select it.
6. The KCeptor model should start rotating, following the physical KCeptor you connected.
!!! note	
	To test the calibration throughly, make several rotations, aligning different axis of the KCeptor with the World axis inside Blender. Hold the KC still for a few seconds and make sure there's no drift.


## Setting KCeptor address

When you want to **chain more than one KCeptor** on the same branch of the Hub++, each KCeptor needs to be assigned a different **address**. This allows for the KCeptors to be correctly recognized.

A KCeptor's address can be set through the [Remote Console](basic-setup.md#accessing-the-remote-console):

1. Connect just one KCeptor to the Hub++.
2. In the Remote Console, select the address you want to set from the dropdown in the section _Set i2c address_ and click **Set i2c address**.

### KCeptor addresses for default biped configuration

Chordata Motion software comes with 2 configuration profiles already defined. Here's how to set your KCeptor addresses in order to use these profiles.

!!! warning
	Depending on the type fo kit you bought, it will contain 2 or 5 KCeptors with a **green or pink dot**. If you have 2, use these for the hands. If you have 5, use them for hands, feet, and head. In general, KCeptors with marked with green or pink dots have to be used as termination of branches.

#### Default biped configuration (no clavicles)

This configuration is designed for capturing human bodies.  
It requires 15 sensors, with these addresses set:

| Quantity | Address | 
|----------|---------|
| 5        | 0x40    |
| 5        | 0x41    |
| 5        | 0x42    |

The following diagram shows how to distribute the sensors for this configuration:
![default biped configuration 15 sensors](img/kceptorpp/kcpp_def_biped15.png){: class="component-photo-wider"}

#### Default biped configuration (with clavicles)

This configuration requires two additional KCeptor to the default one, allowing you to capture clavicles. 
It requires 17 sensors, with these addresses set:

| Quantity | Address |  |
|----------|---------||
| 5        | 0x40    ||
| 5        | 0x41    ||
| 5        | 0x42    | |
| 2        | 0x43    | *additional clavicles sensors* |

The following diagram shows how to distribute the sensors for this configuration:
![default biped configuration 17 sensors](img/kceptorpp/kcpp_def_biped17.png){: class="component-photo-wider"}


## Assemble KCeptor++, KCeptor++ socket and textile fixing
 
Each KCeptor++ needs to be assembled together with these components:

- KCeptor++ socket
- 4 plastic male-male spacers
- Adjustable textile fixing solution
- Rubber layer (*optional*)

Follow these steps to correctly assemble them together.

1. *(optional)* Align the rubber layer to the **socket**.

	![kc-assembly02](img/assembling/kc-assembly02.jpg){: class="component-photo"}
	
	<br>

2. Pass the **textile strap** inside the **socket** (and the rubber), as shown in the images below. Respect the orientation of the strap you see in the images, as the side with rubber lines is meant to be facing inward.	

	![kc-assembly03](img/assembling/kc-assembly03.jpg){: class="component-photo"}

	![kc-assembly04](img/assembling/kc-assembly04.jpg){: class="component-photo"}

	![kc-assembly05](img/assembling/kc-assembly05.jpg){: class="component-photo"}

	<br>

3. Attach the **4 male-male spacers** to the **socket**.

	![kc-assembly07](img/assembling/kc-assembly07.jpg){: class="component-photo"}

	<br>

4. Attach the **KCeptor++** to the **4 male-male spacers**.	
	
	![kc-assembly08](img/assembling/kc-assembly08.jpg){: class="component-photo"}

	<br>
	
5. Attach the buckle to the **textile strap**, as shown in the image below.

	![kc-assembly06](img/assembling/kc-assembly06.jpg){: class="component-photo"}