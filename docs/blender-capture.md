# Blender add-on capture

Apart from the [Remote Console](basic-setup.md#accessing-the-remote-console), another beginner-friendly option for managing Chordata Motion's system and captures, is our [Blender](https://blender.org) Add-on. Leveraging Blender 3D capabilities, Chordata Motion add-on offers a complete toolkit for managing, inspecting and recording captures.


!!! note 
	Accessing motion capture data is made possible by the development of **clients**, softwares that communicate with Chordata Motion's core framework. These can act as **standalone applications** or as **intermediaries** between the core framework and another software. Chordata's Blender add-on is part of the second category, practically making Blender a complete client for the motion capture process.
	

## Add-on installation

Follow these steps to install Chordata Motion Blender add-on:

1. Download and install [Blender](https://www.blender.org/download/).
2. Download the add-on from our [downloads page](https://chordata.cc/downloads/).
3. To install and activate the add-on:
    - Launch Blender.
    - Go to "Edit" > "Preferences..." > "Add-ons".
    - Click the "Install..." button.
    - Select the Chordata Motion add-on .zip file you've downloaded. 
    - Once installed, the add-on will appear in Blender's add-ons list (you can use the search field to find it).
    - Tick the box next to the add-on's name in order to activate it.

![Blender preferences](img/blender/preferences_w_ellipses.png){: class="component-photo"}

## Mocap workspace setup

Once the Blender Add-on is installed we can proceed setting up a basic capture workflow in Blender.

1. Select and delete everything in the scene by pressing the "a" key on your keyboard and then the "x" key. Confirm by clicking "Delete" in the dialogue that pops up.

2. Display the **“Add” menu** either by using the keyboard shortcut
"shift + a" or from the top Header bar.

3. Select "**Chordata**" > "**Add Mocap avatar to the Scene**".
!!! warning
    The first time you use the addon, you will be prompted to install some dependencies. Make sure to have an internet connection. **The installation might take some minutes and  Blender’s UI will freeze while dependecies are installing.** Once the dependencies are installed you will notice changes in the scene. A human avatar will have appeared in the middle of the scene, and there’s also a new “Mocap” tab at the top ot the window.

You can now switch to the newly generated "**Mocap**" workspace.
In this workspace you will find two basic **Chordata Node Tree**s already setup with a 3D humanoid avatar ready for capture. 

Choose which **node tree preset** you're going to use, based on wheater you're using the 15 or 17 sensors configuration.

![presets](img/blender/presets.png){: class="component-photo-widest"}



## Initialize the capture

- Click **Connect** on the **Notochord Node** to initialize the capture.
- The 3D armature should be now moving, but with its parts incorrectly displaced.

![Notochord node](img/blender/notochordnode.png){: class="component-photo"}


## Pose calibration

Pose calibration is **CRUCIAL** for achieving a high quality capture. The calibration is achieved by performing a precise sequence of movements. 

This process is **easier with someone** guiding the person wearing the sensors, informing them about the movements and managing the pose calibration from the Blender add-on.

Calibration is diveded in two main parts: **static calibration** and **functional calibration** (divided in arms, torso and legs calibration).

![armature node](img/blender/armaturenode.png){: class="component-photo"}

For each section of the calibration, press the corresponding button on the **Armature Node** to start the calibration and to finish it.


### Static calibration

The perfomer stands still for a couple seconds, in what is known as an N-Pose.

- Naturally stand up straight, without forcing it
- Arms stretched at the sides of the body
- Palms facing the body
- Looking forward

<video  autoplay="true" loop="true">
	<source src="../img/performer/giulia_calib_move_static.mp4" type="video/mp4">
</video>
<br>



### Functional calibration

The performer follows a sequence of movements.
Try to keep everything as square and parallel as possible, while moving at a natural velocity.

#### Arms calibration

1. Start in N-POSE.
2. Rotate your arms by 90 degrees, pointing forward, while keeping them straight with your palms facing your body.
3. Return to N-POSE.

<video  autoplay="true" loop="true">
	<source src="../img/performer/giulia_calib_move_arms.mp4" type="video/mp4">
</video>
<br>

#### Torso calibration

1. Start in N-POSE.
2. Bend over, torso and head going down as if they were one piece. (**Don't bend your neck** and make sure the head follows the movement without rotating.).
3. Return to N-POSE.

<video  autoplay="true" loop="true">
	<source src="../img/performer/giulia_calib_move_trunk.mp4" type="video/mp4">
</video>
<br>

#### Legs calibration

1. Start in N-POSE.
2. Bring one leg up goes up as far as possible (without forcing it), leg and foot moving as if they were one piece. The foot  should be kept at 90 degrees relative to the leg. Don't bend your ankle.
3. Return to N-POSE.
4. Repeat for the other leg.

<video  autoplay="true" loop="true">
	<source src="../img/performer/giulia_calib_move_legs.mp4" type="video/mp4">
</video>
<br>

## Record capture

To record with Chordata's blender add-on:

1. While in the node tree area, display the **“Add” menu** either by using the keyboard shortcut "shift + a" or from the top Header bar.
2. Add a **Record Node**.
3. Connect the **Armature Out** socket of the **Armature Node** to the **Armature In** socket of the **Record Node**.
4. Hit the **Record button** to start and stop the recording.

![record node](img/blender/recordnode.png){: class="component-photo"}