# Chordata Motion Docs

Welcome to the official documentation of [Chordata Motion](http://chordata.cc), the open-source motion capture system. 
In this documentation we will cover all you need to know to get your system up and running!

If this is your first time setting up your Chordata Motion system, we suggest you start from our [Quickstart](basic-setup.md) section.