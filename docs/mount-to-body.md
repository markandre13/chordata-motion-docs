# Mount to body

This section covers how to mount the sensors to the body of the performer. Make sure you've succesfully [setup the Kecptors](setup-kceptor.md) before proceeding.

!!!note
	We use the word "performer" to indicate the person wearing the suit.


The following diagram gives a good idea of the general distibution of components and straps:


![Textile distribution](img/performer/textile_distribution_chordata.jpg){: class="component-photo-wider"}


## General considerations

Positioning the KCeptors correctly in a real body requires a couple of precautions. The main idea to keep in mind is that each KCeptor is tracking the orientation of the bone (or group of bones) in the human body that better represents a given "**segment**" (which is the academic name for a bone in the virtual skeleton).

Here are some advice to achive good and stable positioning:

- Each **KCeptor should be positioned where the influece of the "Soft tissue" (like muscles) is as small as possible**. That's something easily achievable in segments such as the head, where there's only a thin layer of flesh and skin separating the skull from the surface of the body. The same requires some additional finesse with the base sensor, which has to track the pelvis bone, surrounded by big muscles.  
<br>

- When a KCeptor slips from its original position, the  system has no way to tell that displacement from real body movements. That's why, once a KCeptor is positioned, the straps should be **tightened as much as possible**, so the sensor stays firmly in place.  
<br>

- When placing the sensors on the body, the orientation or position of the KCeptor relative to the body part is **not important**, since it is compensated by the [pose calibration](blender-capture.md#pose-calibration).  
<br>

- Keep in mind that after a few movements the straps will tend to slip a little and find a resting position where they are more stable. **A good idea is to make the performer move freely for some time after positioning all the KCeptors and, after that, tighten the straps again in the position where they moved**.

## Sensor positioning

### Head

It can be placed in the front, side or back of the head. 
A good spot is the middle of the forehead.

### Dorsal

Place this sensor in the middle of your upper back. Try to reduce as much as possible the 
influence of scapulas' motion.

![placeholder](img/mount_to_body/dorsal.jpg){: class="component-photo"}

### Base

Positioning the base sensor can be tricky. **Try to identify the place where the [sacrum](https://en.wikipedia.org/wiki/Sacrum) is closer to the skin surface, right above both gluteous muscles**. This sensor is attached with multiple straps. Make sure to adjust all of them to give the sensor a stable positioning.

![placeholder](img/mount_to_body/base.jpg){: class="component-photo"}

!!!Tip
    The sensors are disturbed by metal objects, and the "belt" also carries the Raspberry and the power bank. **You want to position those as far as possible from the sensor**.

### Clavicles

**These KCeptors are only used when capturing with a 17 sensors configuration.** There are two possible spots where to place these sensors. 

- In the front part of the chest. Right above the actual clavicle bones.
- In the upper part of the shoulders. This is not recommended for people with highly developed Trapezius muscles.

Each of these positions works better with some movements than with others. Try the one that works best for you.  

![placeholder](img/mount_to_body/clavicle.jpg){: class="component-photo"}

### Upperarm

On this part of the body there are a lot of entangled muscles and therefore lots of soft tissue deformation here. **Try to find the spot where the deltoid is inserted between the biceps and triceps**. You should be able to "touch" the humerus bone underneath.

![placeholder](img/mount_to_body/upperarm.jpg){: class="component-photo"}


### Lowerarm

Place the sensor in the middle among the elbow and the wrist. 
On performers with more developed arm muscles try to avoid portions of the segment where the "conical" shape is less pronounced.

!!!Warning
     If you place it too close to the hand you will track much of the rotation that the Radius and Ulna bones do, in order to twist the wrist. This is already tracked by the hand sensor, so try to avoid it.
### Hand

Place the sensor right after the thumb, crossing your palm.

![placeholder](img/mount_to_body/hand.jpg){: class="component-photo"}


### Upperleg

On this part of the body there are voluminous muscles. Place the sensor on the front where the deformation is smaller.



### Lowerleg

As for the placing on the lowerarm, avoid portions of the segment where the "conical" shape is less pronounced or the KCeptor will tend to slip more. 


### Foot

The sensors can be placed with or without shoes. **Make sure the buckles don't touch the ground**, otherwise they will cause the position of the sensor to change.





## Connecting sensors and Hub with cables

### Hub gates

The Hub has 6 gates. The diagram below illustrates which gate to use in a biped configuration:

![placeholder](img/mount_to_body/hub_gates.png){: class="component-photo"}

- The mid-upper gate connects with the upper part of the torso: _dorsal_ and _head_
- The side gates of the upper row connect to the *arms*
- The mid-lower gate connects to the *base*
- The side gates of the lower row connect to the *legs*

### Different cable lenghts

Chordata Motion kits come with cables of two different  lenghts: 0.5m and 1m.

| Quantity | Lenght  | Required for  |
|----------|---------|---------------|
| 10       | 0.5m    |	Default gear |
| 5        | 1m      |	Default gear |
| 2        | 0.5m     | Only required for 17 sensors gear (default + clavicles) |

Longer cables are for connecting distant components. This is intuitive enough for most of them. For example, a long cable is required from the Hub to the upperarms. A short one is sufficient to connect the upperarm to the lowerarm.

A less intuitive spot seems to be the upper trunk. You will normally want a **short** cable from the Hub to the dorsal sensor and a **long** cable from the dorsal sensor to the head sensor. 
