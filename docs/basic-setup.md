# Quickstart
These instructions will guide you through getting your Chordata Motion system up and running. Please read each section carefully.
## Introduction
Chordata Motion is designed to be modular and customizable. Because of this, it's important to understand some fundamentals about its main components before moving on. Please take a look at the [Components Overview](components-overview.md) before you start setting up your suit.  
  
!!! warning
    A **Raspberrypi** and an **SD card** are required for working with our system. These come included only with our Plug&Play system. Models 3B and 4B are tested and recommended. Models A, Zero W or other should also work but haven't been tested by us. If you do use models other than those recommended or need assistance, please post your experience in our [forum](https://forum.chordata.cc/).
    
## Installing notochordOS

NotochordOS is our custom operative system for the Raspberrypi that comes with all the software needed for capturing with Chordata Motion. You can get it from our [downloads](https://chordata.cc/downloads/) page.

### Flashing notochordOS


In order to use **notochordOS** you need to download the system image and flash it onto the SD card you will use with your Raspberrypi.  

If you're not familiar with this process we recommend using [Etcher](https://www.balena.io/etcher), a beginner friendly flashing utility. Once you've downloaded Etcher just follow these steps:  

- Download notochordOS image
- Insert the SD card in your computer’s card reader
- Run Etcher
- Select the downloaded image, select your SD card as destination drive and hit Flash!

![balena_etcher menu](img/os/Etcher-gif.gif)

Wait until the Flash process is completed (it might take a couple of minutes). Once finished you can insert the SD card in your Raspberrypi and boot up your freshly installed system.

## WIFI configuration

### Setting up country
NotochordOS comes with everything you need to start capturing with your Chordata Motion system.  
  
The only step required for allowing the Raspberrypi to correctly connect to your WiFi network is setting your country. The easiest way to achieve it is to add a file to the SD card and let the NotochordOS handle the rest of the configuration. Follow this steps:

- Insert the SD card in your computer
- You will see one or two partitions appearing on your system. Choose the smallest one, which is called `boot`. If doing this on windows you might get a popup warning asking to format the unit, just ignore that
- Add a file to this partition called `countrycode.txt` and write your **TWO LETTER** code inside. You can get a full list of countrycodes [here](https://www.iso.org/obp/ui/#home).

### Manual WIFI country configuration

The same thing can also be achieved manually by connecting the Raspberrypi to a monitor and a keyboard, opening the terminal and executing the command:

```bash
sudo raspi-config
```

Then, using Arrow Keys and Enter, navigate to *Localization Options* -> *Change Wi-fi Country* -> [YOUR COUNTRY] -> *OK* -> *Finish*.

### Hotspot vs external WIFI network

By default the notochord OS will act as a hotspot exposing a WIFI network with SSID: "**Chordata-net**", and password "**chordata**". 

This network is useful for the initial configuration, but for better performance using an external WIFI network is recomended. The notochord OS will look for available external WIFI networks on startup. If it finds one in range it will connect to it and drop the Hotspot functionality.

By default the notochord OS will look for a network with SSID: "**Chordata-ext**" with password: "**chordata**". You can also set custom WIFI networks, see the [set a custom external wifi network section](#set-a-custom-external-wifi-network) below.



## Assembling Raspberrypi, Hub++ and Hub++ socket

Before being able to connect and test the **KCeptors**, we need to assemble the the **Raspberrypi** together with the **Hub++** and the **Hub socket**.  

!!! warning
    Before you start assembling the components, make sure to **power off** your Raspberrypi and disconnect it from the power.

!!! note "You will need"
    - Raspberrypi
    - Hub++
    - Hub socket *(found already mounted on the hips textile strap)*
    - Plastic spacers *(Female-Female and Male-Female)*
    - Plastic screws
    - Screwdriver *(not included)*
  
The components need to be stacked together, from the bottom to the top.  
Follow these steps to correctly assemble all the parts together:

1. Attach **4 Female-Female spacers** to the **Hub socket** with **4 screws**. The screws go below the socket and the spacers above.

    ![hub-rpi-assembly01](img/assembling/hub-rpi-assembly01.jpg){: class="component-photo"}

2. Attach the **Raspberrypi** to the **4 Female-Female spacers**, securing it with **4 Male-Female spacers**.

    ![hub-rpi-assembly01](img/assembling/hub-rpi-assembly02.jpg){: class="component-photo"}

3. Mount the **Hub++** above the **Raspberrypi**. You will need to correctly connect the pin headers of the Raspberrypi to the Hub++. If this isn't done correctly you will notice that the spacers aren't aligned correctly.

    ![hub-rpi-assembly01](img/assembling/hub-rpi-assembly03.jpg){: class="component-photo"}

4. Finally, secure the **Raspberrypi** to the **4 Male-Female spacers** using **4 screws**.

    ![hub-rpi-assembly01](img/assembling/hub-rpi-assembly04.jpg){: class="component-photo"}

## First connection

Once notochordOS, the Raspberrypi and the Hub++ are correctly setup, we can proceed making the first connection to the Raspberrypi.
  
### Accessing the Remote Console

Chordata's **Remote Console** is a web application that gives you easy access to all the basic functions of a Chordata Motion system. 



![Chordata control server](img/control_server/chordata_control_server_main.png)


To access the remote console for the first time:

1. Power on your Raspberrypi with [NotochordOS](#installing-notochordos) and wait for it to boot up.
2. Connect the computer to the network "**Chordata-net**", automatically created by the Raspberrypi. The default password is "chordata".
3. Open your browser and navigate to "http://notochord.local".

!!! tip
    Depending on your networking configuration your device might not be able to find the `notochord.local` domain name. In case the NotochordOS's is acting as a hotspot you can use the static ip: `http://192.168.85.1`

### Testing KCeptors

From the remote console, you can test that the **KCeptors** are correctly detected by the software.

1. Connect two KCeptors to **different ports** of the Hub++, without chaining more than one on the same port.
2. Click the **“Scan”** button in the remote console.
3. If everything works correctly, you should see an output in the terminal window, informing you that the KCeptors you connected were found.

!!! note
    Connecting more than one KCeptor in series to the same port of the Hub++ requires setting up the KCeptor addresses correctly. This is covered in [Set KC++ address](setup-kceptor.md)


### Set a custom external WIFI network

Once you have access to the remote console you can add external WIFI networks for it to connect to. Navigate to the "maintenance" tab, under the "Connection Manager" section you can update the password for existing WIFI networks or add new ones.